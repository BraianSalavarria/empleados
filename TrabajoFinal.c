#include<stdlib.h>
#include<stdio.h>
#include<string.h>
/* cada nodo de este ejemplo va a tener una variable de tipo empleado
    para que tener toda la informacion de un empleado como variable o dato
    que va contener cada nodo.

    cada nodo esta compuesto por un puntero que va a apuntar al siguiente nodo
    y como dato va a tener una variable de tipo empleado.
*/

//definimos la estructura Empleado
typedef struct EMPLEADO{

    char nombre[40];
    char apellido[40];
    int legajo;
    float sueldo;
} Empleado;

//definimos la estructura Nodo
typedef struct NODO_EMPLEADO{

    Empleado empleado;
    struct NODO_EMPLEADO* suguiente; //puntero de tipo NODO_EMPLEADO
}Nodo;

//Prototipos de Funciones

 void Menu(Nodo *nodo_Cabeza);
 Nodo* crear_Nodo(Empleado empleado);
 void insertar_Nodo_Ala_Lista(Nodo **cabeza,Empleado empleado);
 void mostrar_Lista(Nodo *cabeza);
 void metodoBurbuja_ordenaPorMayor(Nodo *cabeza);
 Nodo* eliminar_Nodo(Nodo *lista,int legajo);
 void buscarEmpleado(Nodo *lista,int legajo);
 void modificarEmpleado(Nodo* lista,int legajo);
 void  ordenamientoBurbuja(Nodo *lista);
 void longitud_Lista(Nodo *cabeza);

int main(){
//nodo principal
Nodo * nodo_Cabeza=NULL;
//variables de tipo empleado para el ejemplo
Empleado empleado;
Empleado empleado2;
Empleado empleado3;
Empleado empleado4;
Empleado empleado5;
Empleado empleado6;

empleado.legajo=1254;
empleado.sueldo=23600.00;
empleado2.legajo=1234;
empleado2.sueldo= 37500.00;
empleado3.legajo=4567;
empleado3.sueldo=70500.00;
empleado4.legajo=6789;
empleado4.sueldo=105320.00;
empleado5.legajo=34778;
empleado5.sueldo=15300.00;
empleado6.legajo=789864;
empleado6.sueldo=8500.00;

//strcpy es una funcion que copia un arreglo a otro-lo hice para que no tengas que andar ingresando por teclado a cada uno.

strcpy(empleado.nombre,"Braian");
strcpy(empleado.apellido,"Salavarria");

strcpy(empleado2.nombre,"Carlos");
strcpy(empleado2.apellido,"Salavarria");

strcpy(empleado3.nombre,"Exequiel");
strcpy(empleado3.apellido,"Gordillo");

strcpy(empleado4.nombre,"Nicolas");
strcpy(empleado4.apellido,"Dolz");

strcpy(empleado5.nombre,"Gabriel");
strcpy(empleado5.apellido,"Sigampa");

strcpy(empleado6.nombre,"Angel");
strcpy(empleado6.apellido,"villagran");

//Esta funcion lo que hace es crear un nodo y los enlaza
insertar_Nodo_Ala_Lista(&nodo_Cabeza,empleado);
insertar_Nodo_Ala_Lista(&nodo_Cabeza,empleado2);
insertar_Nodo_Ala_Lista(&nodo_Cabeza,empleado3);
insertar_Nodo_Ala_Lista(&nodo_Cabeza,empleado4);
insertar_Nodo_Ala_Lista(&nodo_Cabeza,empleado5);
insertar_Nodo_Ala_Lista(&nodo_Cabeza,empleado6);

// menu de la funcion-recibe como parametro el primer nodo para poder acceder a los demas
Menu(nodo_Cabeza);

 return 0;
}

Nodo* crear_Nodo(Empleado empleado){ //creamos un nodo que no apunta a ninguno

    Nodo* nuevo_nodo = (Nodo*) malloc(sizeof(Nodo)); //pedimos memoria para nuestro nuevo nodo del tipo "Nodo"
    nuevo_nodo->empleado= empleado;
    nuevo_nodo->suguiente= NULL;

    return nuevo_nodo;
}

void insertar_Nodo_Ala_Lista(Nodo **cabeza,Empleado empleado){ //utilizamos el nodo que creamos en la funcion crear_Nodo y lo insertamos en la lista
																//de esta forma queda en la cabeza y apunta a un nodo siguiente
    Nodo* nuevo_nodo = crear_Nodo(empleado);
          nuevo_nodo->suguiente= *cabeza;
          *cabeza = nuevo_nodo;

}

void mostrar_Lista(Nodo *cabeza){		
 Nodo* PnodoAux = cabeza;

 printf("Detalle de los Empleados: \n\n");

 while(PnodoAux !=NULL){				//mientras Primer nodo auxiliar sea distinto de NULL nos mostrara los datos de los empleados
										//en caso contrario, deja de mostrar porque no tendria a donde a puntar y significaria que el nodo es NULL
    printf("Nombre de Empleado: %s \n",PnodoAux->empleado.nombre);
    printf("Apellido de Empleado: %s \n",PnodoAux->empleado.apellido);
    printf("Nro de Legajo: %d \n",PnodoAux->empleado.legajo);
    printf("Sueldo: %.2f \n\n",PnodoAux->empleado.sueldo);

    PnodoAux= PnodoAux->suguiente;  //apunta al siguiente nodo

 }

}

void Menu(Nodo *nodo_Cabeza){
int opcion=0;

do{
	printf("\t\t\t\t ----------------- \n");
	printf("\t\t\t\t Menu de Empleados \n");
	printf("1)- Mostrar Lista de Empleados (NODOS)  \n");
	printf("2)- Agregar Empleado (NODO)  \n");
	printf("3)- Ordenar Lista (METODO BURBUJA)  \n");
	printf("4)- Borrar Empleado (NODO)  \n");
	printf("5)- Buscar Empleado (NODO) \n");
	printf("6)- Salir  \n\n");
	printf("\t\t\t\t ................. \n");

	printf("Por favor ingrese una opcion valida \nopcion: ");
	scanf("%d",&opcion);
	system("cls");

switch(opcion){

    case 1:{
            mostrar_Lista(nodo_Cabeza);		//muestra los datos precargados y si agregamos un empleado nuevo, tambien lo muestra pero en la cabeza
            break;
            }
    case 2:{

            Empleado empleado;
            printf("Ingrese Nombre: \n");
            scanf("%s",empleado.nombre);			// el case 2 utiliza la funcion insertar nodo, para escribir los datos en el campo empleado 
            printf("Ingrese Apellido: \n");
            scanf("%s",empleado.apellido);
            printf("Ingrese Legajo: \n");
            scanf("%d",&empleado.legajo);
            printf("Ingrese Sueldo: \n");
            scanf("%f",&empleado.sueldo);
            insertar_Nodo_Ala_Lista(&nodo_Cabeza,empleado);

            break;
           }
    case 3:{
            ordenamientoBurbuja(nodo_Cabeza); 		//aqui hacemos uso de la funcion de ordenamiento, la burbuja, 
            break;									//solo ordena, es su funcion unica
            }
    case 4:{
            int legajo;
            printf("Ingrese Legajo de Empleado: \n");	
            scanf("%d",&legajo);
            nodo_Cabeza=eliminar_Nodo(nodo_Cabeza,legajo);		//se elimina el nodo con la funcion free() que borra su puntero
            break;												//liberando espacio de memoria
           }													
    case 5:{
            int legajo;
            printf("Ingrese el Legajo: \n");
            scanf("%d",&legajo);
            buscarEmpleado(nodo_Cabeza,legajo);				//esta funcion consiste en buscar mediante el puntero Lista e ingresando a los atributos
            break;											//de empleado, exactamente al legajo y mediante una comparacion se define si se encuentra
           }
    case 6:{
            opcion =6;
            break;
           }
    default:{
            printf("Opcion Incorrecta, Intente nuevamente \n");
            break;
            }
}

    }while(opcion != 6);

}

Nodo* eliminar_Nodo(Nodo *lista, int legajo){

//preguntar si la lista esta vacia
if(lista != NULL){

    Nodo *aux_borrar;
    Nodo *anterior = NULL;
    aux_borrar=lista;


  //recorrer la lista
  while((aux_borrar != NULL) && (aux_borrar->empleado.legajo != legajo)){

    anterior = aux_borrar;
    aux_borrar = aux_borrar->suguiente;
  }

  //el elemento no ha sido encontrado
  if(aux_borrar == NULL){
    printf("El empleado no se encuentra o no existe \n");
    printf("Pruebe ingresando un legajo valido:\n");
  }

  else if( anterior == NULL){
    lista = lista->suguiente;
    free(aux_borrar);
  }
  //el elemento esta en la lista pero no es el primer nodo
  else{
        anterior->suguiente = aux_borrar ->suguiente;
        free(aux_borrar);
  }

}
return lista;

}

void buscarEmpleado(Nodo *lista,int legajo){

while(lista != NULL){

    	if(lista->empleado.legajo == legajo){
        printf("\n\nDetalle del Empleado (NODO)\n\n");
        printf("Nombre: %s \n",lista->empleado.nombre);
        printf("Apellido: %s \n",lista->empleado.apellido);
        printf("Legajo: %d \n",lista->empleado.legajo);
        printf("Sueldo: %.2f \n",lista->empleado.sueldo);
        printf("\n\n");
        break;
      	}
	  	else
	  	{
          lista=lista->suguiente;
     	}
	}

}

void ordenamientoBurbuja(Nodo *lista){

    Nodo* p = lista;
    Nodo *aux = NULL;
    Empleado emp;
    float s1,s2;

    while(p->suguiente != NULL){
        aux = p->suguiente;

        while(aux != NULL){

                s1 = p->empleado.sueldo;
                s2 = aux->empleado.sueldo;

                if(s1>s2){

                emp = aux->empleado;
                aux->empleado = p->empleado;
                p->empleado = emp;


                }
                aux = aux->suguiente;

            }
            p=p->suguiente;

        }
}




